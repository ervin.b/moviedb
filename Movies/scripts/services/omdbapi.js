var module = angular.module('Movies');
module.factory('omdbapi', ['$http', '$httpParamSerializer', '$q', function ($http, $httpParamSerializer, $q) {

    var apiUrl = "http://www.omdbapi.com/?apikey=XXXXXXXXXXXXXXX";

    var buildUrl = function (url, params) {
        var serializedParams = $httpParamSerializer(params);
        if (serializedParams.length > 0) {
            url += ((url.indexOf('?') === -1) ? '?' : '&') + serializedParams;
        }

        return url;
    }

    var getMovieById = function (id) {
        url = buildUrl(apiUrl, { 'i': id });
        return $http.get(url)
            .then(function (response) {
                console.log(response);
                return response.data;
            });
    }

    var getListOfMoviesById = function (idList) {
        promises = [];
        idList.forEach(element => {
            promises.push($http.get((buildUrl(apiUrl, { 'i': element }))));

        });

        return $q.all(promises)
    }

    var getMovieByTitle = function (title) {
        url = buildUrl(apiUrl, { 't': title });
        return $http.get(url)
            .then(function (response) {
                return response.data;
            });
    }

    var searchMovie = function (title, type, year, pageNum) {
        url = buildUrl(apiUrl, { 's': title, 'type': type, 'y': year, 'page': pageNum });
        return $http.get(url)
            .then(function (response) {
                return response.data;
            });
    }

    return {
        getMovieById: getMovieById,
        searchMovie: searchMovie,
        getMovieByTitle: getMovieByTitle,
        getListOfMoviesById: getListOfMoviesById
    };

}]);
