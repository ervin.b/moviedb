movieApp = angular.module("Movies", ['ngRoute','ngStorage'])
    .config(function($routeProvider) {
        $routeProvider.when('/',
        {
            templateUrl:'templates/home.html',
            controller: 'HomeController'  
        });

        $routeProvider.when('/movie/:id',
        {
            templateUrl:'templates/movie.html',
            controller: 'MovieController'
        });
        
        $routeProvider.when('/search',
        {
            templateUrl:'templates/search.html',
            controller: 'SearchController'
        });
        
        $routeProvider.when('/favourites',
        {
            templateUrl: 'templates/favourites.html',
            controller: 'FavouritesController'
        });
        
        $routeProvider.otherwise({redirectTo:'/'});

    });