movieApp.directive("movieRating", function () {
    return {
        restrict: 'E',
        templateUrl: '/templates/movie-rating.html',
        scope: {
            movie:"=movie"
        }

    }
});

movieApp.directive("ratingSrc", function () {
    return {
        restrict: 'A',
        link: function(scope, element, attr) {
            if(attr.ratingSrc == "Internet Movie Database") {
                element.attr("src", "/images/logo-imdb.svg");
            } else if(attr.ratingSrc == "Rotten Tomatoes") {
                element.attr("src", "/images/rt.png");
            } else if(attr.ratingSrc == "Metacritic") {
                element.attr("src", "/images/Metacritic.png")
            }
        },
    }
});