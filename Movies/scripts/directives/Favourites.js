movieApp.directive("favouriteList", function () {
    return {
        restrict: 'E',
        templateUrl: '/templates/favourites-list.html',
        scope: {
            movies:"=",
        },
        link:function(scope, element, attrs) {
            scope.$watch('movies', function(newValue) {
                if(newValue !== undefined) {
                    scope.movies = newValue;
                }
            });
        }
    }
});