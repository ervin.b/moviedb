movieApp.controller('MovieController', ['$scope', '$routeParams','$localStorage','$sessionStorage', 'omdbapi',
    function($scope, $routeParams, $localStorage, $sessionStorage, omdbapi) {
        var movieId = $routeParams.id;
        $scope.$storage = $localStorage;
        $scope.$storage = $localStorage.$default({
            favourites: '[]'
        });

        var onMovieComplete = function(data) {
            if(data.Response === "False") {
                $scope.movies = null;
                $scope.error = data.Error;
            } else {
                $scope.error = null;
                $scope.movie = data;
            }
        };

        var onError = function(reason) {
          $scope.error = "Error while getting a movie.";
          console.log("Movie loading error: " + reason);
        }

        $scope.getById = function(id) {
            omdbapi.getMovieById(id).then(onMovieComplete, onError);
        }

        $scope.addToFavourites = function(id) {
            console.log("Adding to favourites");
            items = JSON.parse($scope.$storage.favourites);
            items.push(id);
            $scope.$storage.favourites = JSON.stringify(items);
        }

        $scope.removeFromFavourites = function(id) {
            items = JSON.parse($scope.$storage.favourites);
            var index = items.indexOf(id);
            if(index > -1) {
                items.splice(index,1);
            }
            $scope.$storage.favourites = JSON.stringify(items);
        }

        $scope.isInLocalStorage = function(id) {
            items = JSON.parse($scope.$storage.favourites);
            var result = false;
            items.forEach(function(item) {
                if(id === item) {
                    result = true;
                }
            });
            return result;
        }

        $scope.getById(movieId);
}]);