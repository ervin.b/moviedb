movieApp.controller('SearchController',
    function ($scope, $location, omdbapi) {
        $scope.pageNum = 1;
        $scope.Math = window.Math;

        var onSearchComplete = function (data) {
            if (data.Response === "False") {
                $scope.error = data.Error;
                $scope.movies = null;
            } else {
                $scope.error = null;
                $scope.movies = data;
                $scope.totalResults = $scope.movies.totalResults;
            }
        }

        var onError = function (reason) {
            $scope.error = "Error while searching for movies";
            console.log("Search error: " + reason);
        }

        $scope.startSearch = function () {
            if ($scope.title === "") {
                $scope.error = "Title is empty";
            } else {
                omdbapi.searchMovie($scope.title, $scope.type, $scope.year, $scope.pageNum)
                    .then(onSearchComplete, onError);
            }
        }

        $scope.movieDetails = function (imdbID) {
            $location.path("/movie/" + imdbID);
        }

        $scope.nextPage = function () {
            if ($scope.pageNum + 1 > $scope.totalResults / 10) {
                $scope.lastPage = true;
            } else {
                $scope.lastPage = false;
                $scope.pageNum++;
                $scope.startSearch();
            }
        }

        $scope.previousPage = function () {
            if ($scope.pageNum - 1 === 0) {
                $scope.firstPage = true;
            } else {
                $scope.firstPage = false;
                $scope.pageNum--;
                $scope.startSearch();
            }
        }
    });