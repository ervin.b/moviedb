movieApp.controller('FavouritesController', ['$scope', '$localStorage', '$sessionStorage','$location', 'omdbapi',
    function ($scope, $localStorage, $sessionStorage, $location, omdbapi) {
        $scope.movies = [];
        $scope.$storage = $localStorage;
        $scope.$storage = $localStorage.$default({
            favourites: '[]'
        });

        var onMovieComplete = function (response) {
            response.forEach(element => {
                $scope.movies.push(element.data);
            });
        };

        var onError = function (reason) {
            $scope.error = "Error while getting a movie.";
            console.log("Movie loading error: " + reason);
        }

        var getListFromLocalStorage = function () {
            items = JSON.parse($scope.$storage.favourites);
            return items;
        }

        $scope.getAllFavourites = function () {
            omdbapi.getListOfMoviesById(getListFromLocalStorage()).then(
                onMovieComplete,onError);
        }

        $scope.openMovie = function(imdbID) {
            $location.path = "/movie/"+imdbID;
        }

        $scope.getAllFavourites();
    }]);