movieApp.controller('HomeController',
    function($scope, omdbapi) {
        $scope.movieList = ['tt0088763', 'tt0133093', 'tt0944947'];
        $scope.movies = [];

        var onMovieComplete = function (response) {
            response.forEach(element => {
                $scope.movies.push(element.data);
            });
        };

        var onError = function (reason) {
            $scope.error = "Error while getting a movie.";
            console.log("Movie loading error: " + reason);
        }

        $scope.getAll = function () {
            omdbapi.getListOfMoviesById($scope.movieList).then(
                onMovieComplete,onError);
        }

        $scope.getAll();
    });